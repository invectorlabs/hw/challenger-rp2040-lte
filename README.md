# The Challenger RP2040 LTE
![Challenger RP2040 LTE](https://ilabs.se/wp-content/uploads/2021/08/iso1-11-scaled.jpg)

## Challenger RP2040 LTE information
The Challenger RP2040 LTE board is an Arduino/Circuitpython compatible Adafruit Feather format micro controller board packed with loads of functionality for your projects that requires an LTE connection.

The main controller of this board is the Raspberry Pi Pico (RP2040) with 8MByte of FLASH memory and 264KByte of SRAM. The RP2040 MCU is built around the dual 32-bit ARM® Cortex™-M0 CPU's running at up to 133 MHz. It has numerous digital peripherals and interfaces such as high speed SPI and QSPI for interfacing to external flash and a full speed USB device for data transfer and power supply for battery recharging.

## Challenger RP2040 LTE Key features
- Dual ARM® Cortex M0 running at up to 133MHz
- 8 MB Flash, 264 KB RAM
- 4G UBlox LTE Modem
- UART, SPI, TWI, I2S
- PWM
- 12-bit ADC
- USB 2.0

## The board

### LTE solution
The SARA-R4 series modules are a multi-band LTE-M / NB-IoT / EGPRS multi-mode solution in the miniature SARA LGA form factor (26.0 x 16.0 mm, 96-pin). They allow an easy integration into compact designs and a seamless drop-in migration from other u-blox cellular module families.

SARA-R4 series modules provide software-based multi-band configurability enabling international multi-regional coverage in LTE-M / NB-IoT and (E)GPRS radio access technologies. SARA-R4 series modules offer data communications over an extended operating temperature range of –40 °C to +85 °C, with low power consumption, and with coverage enhancement for deeper range into buildings and basements (and underground with NB-IoT).

### Power
There is an onboard low power LDO that runs the onboard MCU part of the electronics. This LDO is connected directly to the input power (USB connector and USB/5V pin) but can be disabled by pulling the EN pin low.

The modem have two separate power sources, one for when the device is running on an external 5V power source such as the USB connector or the USB/5V pin on the rightmost header. This consists of a low quiescent DC/DC regulator that can be disabled from the MCU. The second power source is an ideal diode solution which connects the battery power from the battery connector directly to the Modem. The enable/disable pin is of this diode solution is connected to the same enable pin as the DC/DC supply.

The board comes with a standard battery connector for external LiPo batteries as well as a LiPo charger circuit that can charge the connected battery.

### Other stuff
It also has the Challenger standard USB type C connector with ajoining LED's. A red LED for the charging circuit indicating when the attached battery is being charged and a green user programmable LED.

And of course, there is a reset switch and a boot mode button. The board can be put into flash mode in two different ways. The first and simplest way is the 1200baud boot option. This is used by for instance the Arduino environment to automatically put the board into UF2 flash mode for flashing the device. The second way is to hold the boot button while pressing the reset button and then releasing the boot button again. This will also put the device into UF2 flash mode allowing you to send your firmware to the device.

### Pinout
<img src="https://ilabs.se/wp-content/uploads/2021/08/challenger-rp2040-lte-pinout-diagram-v0.1-1024x726.png" alt="drawing"/>

### Software support
The board is supported by both the Arduino environment as well as Circuitpython from Adafruit.

## License
This design covered by the CERN Open Hardware Licence v1.2 and a copy of this license is also available in this repo.

## iLabs
<img src="https://ilabs.se/wp-content/uploads/2021/07/cropped-Color-logo-no-background.png" alt="drawing" width="200"/>

### About us
Invector Labs is a small Swedish engineering company that designs and build electronic devices for hobbyists as well as small and medium sized businesses.

For more information about this board you can visit the product page at our [website](https://ilabs.se/product/challenger-rp2040-lte/)

Questions about this product can be addressed to <oshwa@ilabs.se>.

/* EOF */
